package com.scene_navigation;

import com.scene_navigation.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        launch(args);
    }

    private Stage primarStage;
    NavigationPage1 scPage1;

    NavigationPage1 navigationPage1;
    NavigationPage2 navigationPage2;
    NavigationPage3 navigationPage3;

    Scene navigationscene1;
    Scene navigationscene2;
    Scene navigationscene3;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primarStage = primaryStage;
        navigationPage1 = new NavigationPage1(this);
        navigationPage2= new NavigationPage2(this);
        navigationPage3= new NavigationPage3(this);
        
        navigationscene1 = new Scene(navigationPage1.getNavigationPage1());
        navigationscene2 = new Scene(navigationPage2.getNavigationPage2());
        navigationscene3 = new Scene(navigationPage3.getNavigationPage3());
        
        primaryStage.setScene(navigationscene1);
        primaryStage.setFullScreen(true);
        primaryStage.show();
    }

    public void navigatetopage1() {
        primarStage.setScene(navigationscene1);
        primarStage.setFullScreen(true);
        primarStage.show();
    }

    public void navigatetopage2() {
        primarStage.setScene(navigationscene2);
        primarStage.setFullScreen(true);
        primarStage.show();
    }

    public void navigatetopage3() {
        primarStage.setScene(navigationscene3);
        primarStage.setFullScreen(true);
        primarStage.show();
    }

}