package com.scene_navigation;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

public class NavigationPage2 {

    private HBox hBox;
    private Main app;

    NavigationPage2(Main app) {
        this.app = app;
        create();
    }

    public void create() {
       TextField text = new TextField();
        text.setPromptText("page 2");
        text.setFocusTraversable(false);

        Button button1 = new Button("Next");
        button1.setOnAction(e -> app.navigatetopage3());
        
        Button button2 = new Button("Privious");
        button2.setOnAction(e->app.navigatetopage1());
        
        Button button3 = new Button("Clear");
        button3.setOnAction(e->text.clear());

        hBox = new HBox(20, text, button1, button2,button3);
              hBox.setAlignment(Pos.CENTER);
    }

    public HBox getNavigationPage2() {
        return hBox;
    }

}
