package com.scene_navigation;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

public class NavigationPage3 {
    HBox hBox;
    Main app;

    NavigationPage3(Main app){
        this.app = app;
        create();
    }

    public void create() {


        TextField text = new TextField();
        text.setPromptText("page 3");
        text.setFocusTraversable(false);
       
        Button button1 = new Button("Privious");
        button1.setOnAction(e->app.navigatetopage2());

        Button button2 = new Button("Clear");
        button2.setOnAction(e->{
            text.clear();
        });

        hBox = new HBox(20, text, button1, button2);
              hBox.setAlignment(Pos.CENTER);
    }

    public HBox getNavigationPage3() {
        return hBox;
    }
}
